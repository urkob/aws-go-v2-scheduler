cd functions

for d in */ ; do
    cd $d
    pwd
    go get -u
    rm -rf vendor
    go mod tidy
    go mod vendor
    cd ..
done
cd ..