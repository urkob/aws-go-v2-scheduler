
#!/bin/bash
cd functions
for var in "$@"
do
    cd "$var"
    go mod vendor
    cd ..
done
cd ..