#!/bin/bash


runDevEnv() {

    echo "Executing test coverage"
    ## -race and coverage use it for local testing
    go test ./... -short -race -coverprofile coverreport.out
    go tool cover -html coverreport.out
    go tool cover -func coverreport.out | grep total:
        
    echo "Executing golangci-lint run -E gosec..."
    golangci-lint run -E gosec
    echo "Done golangci-lint run -E gosec..."

    echo "Executing goreportcard-cli"
    goreportcard-cli
    echo "Done goreportcard-cli"
}


cd functions

for d in */ ; do
    
    cd $d
        
    if [ $# -eq 1 ]
    then
    if [ "$1" == "prod" ]
    then
    ## use it for production
    go test ./... -race -coverprofile coverage.out -json > report.json
    else
        runDevEnv
    fi
    else
    runDevEnv
    fi
    cd ..
done

