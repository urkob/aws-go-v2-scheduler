package handler

import (
	"scheduleBusinessRule/model"
)

//HdlrInterface aws handler interface
type HdlrInterface interface {
	Handler(sc model.Schedule) (*model.HandleOutputResult, error)
}
