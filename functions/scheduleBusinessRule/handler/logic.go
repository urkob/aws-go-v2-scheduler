package handler

import (
	"scheduleBusinessRule/model"
	"scheduleBusinessRule/services"

	"github.com/go-playground/validator/v10"
)

type handlerService struct {
	srv      services.SchedulerInterface
	validate *validator.Validate
}

//NewHandlerService creates a new handler service
func NewHandlerService(srv services.SchedulerInterface) HdlrInterface {
	validate := validator.New()
	return &handlerService{
		srv,
		validate,
	}
}

//Handler aws handler method
func (hs handlerService) Handler(sc model.Schedule) (*model.HandleOutputResult, error) {
	return hs.srv.AddRuleLambdaTargets(sc)
}
