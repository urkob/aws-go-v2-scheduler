package main

import (
	"scheduleBusinessRule/config"
	"scheduleBusinessRule/handler"
	"scheduleBusinessRule/services"

	cloudwatchservice "github.com/assetcaresystems/aws-go-wrapper/services/cloudwatch"
	configservice "github.com/assetcaresystems/aws-go-wrapper/services/config"
	lambdaservice "github.com/assetcaresystems/aws-go-wrapper/services/lambda"

	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	cfg := config.LoadData()

	cfgClient := configservice.NewConfigClient()
	cwSrv := cloudwatchservice.NewCloudWatchService(cfgClient)
	lambdaSrv := lambdaservice.NewLambdaService(cfgClient)

	srv := services.NewSchedulerService(cwSrv, lambdaSrv, cfg.Region, cfg.AccountID)

	handlerSrv := handler.NewHandlerService(srv)

	lambda.Start(handlerSrv.Handler)
}
