package model

import (
	"github.com/aws/aws-sdk-go-v2/service/cloudwatchevents"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
)

// Event represents the information for a new event
type Event struct {
	Details []struct {
		Key   string `json:"Key"`
		Value string `json:"Value"`
	} `json:"Details"`
	DetailType string `json:"DetailType"`
	Source     string `json:"Source"`
}

//HandleOutputResult HandleOutputResult
type HandleOutputResult struct {
	TargetOutput        *cloudwatchevents.PutTargetsOutput `json:"targetOutput"`
	RuleOutput          *cloudwatchevents.PutRuleOutput    `json:"ruleOutput"`
	AddPermissionOutput *lambda.AddPermissionOutput        `json:"addPermissionOutput"`
}

//Schedule Schedule
type Schedule struct {
	Consumer struct {
		Name  string `json:"name"`
		Input string `json:"input"`
		Arn   struct {
			Service      string `json:"service"`
			ResourceType string `json:"resourceType"`
		} `json:"arn"`
	} `json:"consumer"`
	Rule struct {
		EventBus           string `json:"eventBus"`
		RuleName           string `json:"ruleName"`
		RuleDescription    string `json:"ruleDescription"`
		ScheduleExpression string `json:"scheduleExpression"`
	} `json:"rule"`
	Permissions struct {
		Action    string `json:"action"`
		Principal string `json:"principal"`
	} `json:"permissions"`
}

func (sc Schedule) GetARN(region string, accountID string) string {
	return "arn:aws:" + sc.Consumer.Arn.Service + ":" + region + ":" + accountID + ":" + sc.Consumer.Arn.ResourceType + ":" + sc.Consumer.Name
}
