package config

import (
	"os"
)

// Config defines configuration parameters
type Config struct {
	AccountID string
	Region    string
}

// LoadData loads environment parameters
func LoadData() Config {
	var cnf Config
	cnf.loadEnvironmentParameters()
	return cnf
}

func (cnf *Config) loadEnvironmentParameters() {
	cnf.AccountID = os.Getenv("ACCOUNT_ID")
	cnf.Region = os.Getenv("REGION")
}
