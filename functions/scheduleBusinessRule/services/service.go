package services

import (
	"scheduleBusinessRule/model"
)

//SchedulerInterface SchedulerInterface
type SchedulerInterface interface {
	AddRuleLambdaTargets(model.Schedule) (*model.HandleOutputResult, error)
}
