package services

import (
	"context"
	"scheduleBusinessRule/model"

	cloudwatchservice "github.com/assetcaresystems/aws-go-wrapper/services/cloudwatch"
	lambdaservice "github.com/assetcaresystems/aws-go-wrapper/services/lambda"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatchevents"
	"github.com/aws/aws-sdk-go-v2/service/cloudwatchevents/types"
	"github.com/aws/aws-sdk-go-v2/service/lambda"
)

type scheduleService struct {
	cwAPI     cloudwatchservice.CWEventsAPI
	lambdaAPI lambdaservice.LambdaAPI
	region    string
	accountID string
}

//NewSchedulerService scheduler service
func NewSchedulerService(cwAPI cloudwatchservice.CWEventsAPI, lambdaAPI lambdaservice.LambdaAPI, region string, accountID string) SchedulerInterface {
	return &scheduleService{
		cwAPI,
		lambdaAPI,
		region,
		accountID,
	}
}

func (srv scheduleService) AddRuleLambdaTargets(sc model.Schedule) (*model.HandleOutputResult, error) {
	rule := &cloudwatchevents.PutRuleInput{
		Name:               aws.String(sc.Rule.RuleName),
		Description:        aws.String(sc.Rule.RuleDescription),
		EventBusName:       aws.String(sc.Rule.EventBus),
		ScheduleExpression: aws.String(sc.Rule.ScheduleExpression),
		State:              types.RuleStateEnabled,
	}

	//1 Add Rule
	outputRuleResult, err := srv.cwAPI.PutRule(context.TODO(), rule)
	if err != nil {
		return nil, err
	}

	lambdaPermissions := &lambda.AddPermissionInput{
		Action:       aws.String(sc.Permissions.Action),
		FunctionName: aws.String(sc.Consumer.Name),
		Principal:    aws.String(sc.Permissions.Principal),
		StatementId:  aws.String("permission-" + sc.Rule.RuleName),
		SourceArn:    outputRuleResult.RuleArn,
	}

	//2 Add lambda permission
	addPermResult, err := srv.lambdaAPI.AddPermission(context.TODO(), lambdaPermissions)
	if err != nil {
		return nil, err
	}

	target := &cloudwatchevents.PutTargetsInput{
		Rule: rule.Name,
		Targets: []types.Target{
			types.Target{
				Id:    aws.String(*rule.Name + "-target"),
				Arn:   aws.String(sc.GetARN(srv.region, srv.accountID)),
				Input: aws.String(sc.Consumer.Input), //Add input if its neccesary
			},
		},
		EventBusName: aws.String(sc.Rule.EventBus),
	}

	//3 Add target to rule
	outputPutTargetResult, err := srv.cwAPI.PutTargets(context.TODO(), target)
	if err != nil {
		return nil, err
	}

	return &model.HandleOutputResult{
		TargetOutput:        outputPutTargetResult,
		RuleOutput:          outputRuleResult,
		AddPermissionOutput: addPermResult,
	}, nil
}
